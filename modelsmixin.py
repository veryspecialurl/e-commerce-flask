from ecommerce import db
from datetime import datetime
from slugify import slugify
import logging


class Base():
    id = db.Column(db.Integer, primary_key=True)
    date_making = db.Column(db.DateTime)

    def save(self):
        self.slug = slugify(self.name)
        self.date_making = datetime.today()
        db.session.add(self)
        db.session.commit()
        logging.info(f'{__name__} {self.name} created')

    def __repr__(self):
        return f'{__name__} {self.id}'
