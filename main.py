from ecommerce import app, db
from auth import auth
import views

db.create_all()

app.register_blueprint(auth,  url_prefix='/auth')

if __name__ == '__main__':
    app.run(debug=True)
