from flask import render_template, request, Blueprint
from ecommerce import app
from forms import AuthForm
from products.models import *


products = Blueprint('products', __name__, template_folder='templates')

@products.route('/')
def main_page():
    form = AuthForm(request.form)
    categories = MainCategory.query.all()
    return render_template('products/main_page.html', form=form, categories=categories)