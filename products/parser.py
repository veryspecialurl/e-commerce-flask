"""
Citilink products parser
Парсит только одну группу, создавая главные группы и подгруппы
"""
from products.models import *
import logging
import json

logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.INFO)


def normalize_str(str):
    return ' '.join(str.strip().replace('\n', '').split())


# import products.parser as par

def MakeProducts(filename='products_parser.json'):
    with open(filename, 'r') as file:
        categories = json.load(file)
        for category in categories:
            if categories[category]['have_primary']:
                cat = MainCategory.query.filter_by(name=category).first()
                if cat is None:
                    main_category = MainCategory(name=category, have_primary=True)
                    main_category.save()
                    logging.info(f'MainCategory:{category} created')
                    categories[category].pop('have_primary')
                    for primary_group in categories[category]:
                        make_products(True, primary_group, main_category, categories[category][primary_group])
            else:
                make_products(False, category, None, categories[category])
    logging.info('Done!')


def make_products(is_primary, group_name, main_cat, dict):
    if not is_primary:
        dict.pop('have_primary')
        category = MainCategory.query.filter_by(name=group_name).first()
        if category is None:
            main_category = MainCategory(name=group_name)
            main_category.save()
            logging.info(f'MainCategory:{group_name} created')
            for product in dict:
                prod = Product.query.filter_by(name=product).first()
                if prod is None:
                    if dict[product]['url'] != None:
                        product = Product(
                            name=product,
                            price=dict[product]['cost'],
                            image_url=dict[product]['url'],
                            description=dict[product]['description'],
                            main_category=main_category
                        )
                        product.save()
                    else:
                        product = Product(
                            name=product,
                            price=dict[product]['cost'],
                            image_url=None,
                            description=dict[product]['description'],
                            main_category=main_category
                        )
                        product.save()
                    logging.info(f'Product:{product} created')
    else:
        category = PrimaryCategory.query.filter_by(name=group_name).first()
        if category is None:
            primary_category = PrimaryCategory(name=group_name, main_group=main_cat)
            primary_category.save()
            logging.info(f'PrimaryCategory:{group_name} created')
            for product in dict:
                prod = Product.query.filter_by(name=product).first()
                if prod is None:
                    if dict[product]['url'] != None:
                        product = Product(
                            name=product,
                            price=dict[product]['cost'],
                            image_url=dict[product]['url'],
                            description=dict[product]['description'],
                            primary_category=primary_category
                        )
                    else:
                        product = Product(
                            name=product,
                            price=dict[product]['cost'],
                            image_url=None,
                            description=dict[product]['description'],
                            primary_category=primary_category
                        )
                    product.save()
                    logging.info(f'Product:{product} created')
