from modelsmixin import Base
from ecommerce import db

class MainCategory(db.Model, Base):
    __tablename__= 'MainCategory'
    name = db.Column(db.String(200), unique=True)
    have_primary = db.Column(db.Boolean, default=False)
    slug = db.Column(db.String(400))
    products = db.relationship('Product', backref='main_category', lazy=True)
    primary_category = db.relationship('PrimaryCategory', backref='main_group', lazy=True)

class PrimaryCategory(db.Model, Base):
    __tablename__ = 'PrimaryCategory'
    name = db.Column(db.String(200), unique=True)
    slug = db.Column(db.String(400))
    products = db.relationship('Product', backref='primary_category', lazy=True)
    main_category_id = db.Column(db.Integer, db.ForeignKey('MainCategory.id'), nullable=False)


class Product(db.Model, Base):
    name = db.Column(db.String(200), unique=True)
    price = db.Column(db.Integer)
    description = db.Column(db.Text)
    image_url = db.Column(db.Text)
    slug = db.Column(db.String(400))
    main_category_id = db.Column(db.Integer, db.ForeignKey('MainCategory.id'), nullable=True)
    primary_category_id = db.Column(db.Integer, db.ForeignKey('PrimaryCategory.id'), nullable=True)
