from wtforms import Form, StringField, PasswordField, validators

class RegisterForm(Form):
    email = StringField('Email', [validators.length(min=5), validators.email('Введите корректный Email')])
    password = PasswordField('Password', [validators.length(min=8, message='Минимальная длина пароля 8 символов')])
    password2 = PasswordField('Password2', [validators.EqualTo('password', message='Пароли должны сопадать')])
    address = StringField('Address', [validators.length(min=6, message='Минимальная длина адреса 6 символов')])
    index = StringField('Index', [validators.length(min=6, message='Минимальная длина индекса 6 символов')])

class AuthForm(Form):
    email = StringField('Email', [validators.length(min=5), validators.email('Введите корректный Email')])
    password = PasswordField('Password', [validators.length(min=8, message='Минимальная длина пароля 8 символов')])