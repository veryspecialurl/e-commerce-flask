from flask import render_template, request
from ecommerce import app
from auth.auth import auth
from products.products import products

app.register_blueprint(auth,  url_prefix='/auth')
app.register_blueprint(products)


