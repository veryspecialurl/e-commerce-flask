from flask import flash
from ecommerce import db
from passlib.hash import pbkdf2_sha256
from flask_login import login_user
from modelsmixin import Base

class Users(db.Model, Base):
    email = db.Column(db.String(200), unique=True)
    password = db.Column(db.String(200))
    address = db.Column(db.String(200))
    index = db.Column(db.Integer)
    is_active = db.Column(db.Boolean, default=True)
    is_admin = db.Column(db.Boolean, default=False)

    def save(self):
        self.password = pbkdf2_sha256.hash(self.password)
        db.session.add(self)
        db.session.commit()

    def login(self, password):
        if pbkdf2_sha256.verify(password, self.password):
            login_user(self)
        else:
            flash('Неправильный пароль')

    def get_id(self):
        return self.id

    def is_authenticated(self):
        return True
