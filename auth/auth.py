from flask import request, render_template, flash, url_for, redirect, Blueprint
from auth.models import Users
from flask_login import login_user, logout_user
from forms import RegisterForm, AuthForm

auth = Blueprint('auth', __name__, template_folder='templates')

@auth.route('/register', methods=['GET', 'POST'])
def register():
    form = RegisterForm(request.form)
    if request.method == 'POST' and form.validate():
        new_user = Users(
            email=form.email.data,
            password=form.password.data,
            address=form.address.data,
            index=form.index.data
        )
        new_user.save()
        login_user(new_user)
        return redirect(url_for('main_page'))
    return render_template('auth/register.html', form=form)


@auth.route('/login', methods=['GET', 'POST'])
def login():
    form = AuthForm(request.form)
    if request.method == 'POST':
        user = Users.query.filter_by(email=form.email.data).first()
        if user is not None:
            user.login(form.password.data)
            return redirect(url_for('main_page'))
        else:
            flash('Пользователь с таким именем отсутствует')
    return render_template('auth/login.html', form=form)

@auth.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('main_page'))