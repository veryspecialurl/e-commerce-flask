from config import settings
from flask import Flask
from flask_script import Manager
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from flask_migrate import Migrate, MigrateCommand

app = Flask(__name__)
app.config.from_object(settings)

db = SQLAlchemy(app)
from views import *
from auth.models import *
from products.models import *
migrate = Migrate(app, db)

manager = Manager(app)

@manager.command
def createsuperuser():
    email = input('Email:')
    password = input('Password:')
    user = Users(email=email, password=password, address=None, index=None, is_admin=True)
    user.save()
    print(f'SuperUser: {email} created.')

manager.add_command('db', MigrateCommand)

login_manager = LoginManager()

@login_manager.user_loader
def load_user(user_id):
    return Users.query.get(user_id)

login_manager.init_app(app)


if __name__ == '__main__':
    manager.run()

