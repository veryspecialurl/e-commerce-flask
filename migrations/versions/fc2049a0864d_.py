"""empty message

Revision ID: fc2049a0864d
Revises: c54f8c93b446
Create Date: 2020-01-04 23:18:11.759296

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'fc2049a0864d'
down_revision = 'c54f8c93b446'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('MainCategory', sa.Column('slug', sa.String(length=400), nullable=True))
    op.add_column('PrimaryCategory', sa.Column('slug', sa.String(length=400), nullable=True))
    op.add_column('product', sa.Column('slug', sa.String(length=400), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('product', 'slug')
    op.drop_column('PrimaryCategory', 'slug')
    op.drop_column('MainCategory', 'slug')
    # ### end Alembic commands ###
