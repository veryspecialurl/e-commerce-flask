from flask import Flask
from config import settings
from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy
from flask_script import Manager, Command
from flask_migrate import Migrate, MigrateCommand
from models import Users

app = Flask(__name__)
app.config.from_object(settings)

db = SQLAlchemy(app)
migrate = Migrate(app, db)
#Хули не работаешь?!
class CreateUser(Command):

    def createsuperuser(self):
        self.email = input()
        self.password = input()
        user = Users(email=self.email, password=self.password, address=None, index=None, is_admin=True)
        user.save()
        print(f'SuperUser: {self.email} created.')

manager = Manager(app)

manager.add_command('db', MigrateCommand)
manager.add_command('users', CreateUser())

login_manager = LoginManager()
login_manager.init_app(app)

if __name__ == '__main__':
    manager.run()